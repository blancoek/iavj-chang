#version 330 core
in vec2 TexCoord;
out vec4 color;

uniform sampler2D image;
uniform vec4 oColor;

void main()
{    
	color = texture(image, TexCoord);
}  