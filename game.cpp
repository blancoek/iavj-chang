#include "headers/game.h"



PathFollowing* splineBehaviour;
Model* ghostmodel, *ghostmodel2;

Game::Game(GLuint width, GLuint height, float maxSpeed, float maxAcceleration) : Width(width), Height(height), maxSpeed(maxSpeed), maxAcceleration(maxAcceleration){
};

Game::~Game() { };
	// Initialize game state (load all shaders/textures/levels)
void Game::setCamera(Camera& camera) {
		this->camera = &camera;
	}

void Game::Init() {
	srand(time(NULL));
	// Load shaders
	ResourceManager::LoadShader("shaders/shader.vs", "shaders/shader.fs", nullptr, "box");
	ResourceManager::LoadShader("shaders/Splines/shader.vs", "shaders/Splines/shader.fs", nullptr, "lines");
	ResourceManager::LoadShader("shaders/Model/shader.vs", "shaders/Model/shader.fs", nullptr, "model");

	// camera/view transformation
	glm::mat4 projection = glm::perspective(glm::radians(camera->Zoom), (float)Width / (float)Height, 0.1f, 100.0f);
	ResourceManager::GetShader("box").Use().setMat4("projection", projection);
	glm::mat4 view = camera->GetViewMatrix();
	ResourceManager::GetShader("box").Use().setMat4("view", view);

	
	//ghostmodel = new Model("models/sw/toilet.obj");
	ghostmodel = new Model("models/ghost/GhostStill/GhostDiffuse.obj");
	ghostmodel2 = new Model("models/BossGhost/BossGhostStill/BossGhost.obj");
	
	// Load textures
	ResourceManager::LoadTexture("resources/container.jpg", GL_FALSE, "box");

	// Generate and load Meshes
	CubeMesh* boxMesh = new CubeMesh(ResourceManager::GetShader("box"));
	ResourceManager::LoadMesh(boxMesh, "box");
	boxMesh->setTexture(ResourceManager::GetTexture("box"));

	CubeMesh* wallMesh = new CubeMesh(ResourceManager::GetShader("box"));
	ResourceManager::LoadMesh(wallMesh, "box");
	boxMesh->setTexture(ResourceManager::GetTexture("box"));

	//Walls
	
	GameObject* wall1 = new GameObject();
	wall1->kinematic.Position = glm::vec3(-8.0f, 0.0f, 5.0f);
	wall1->setModel(ghostmodel);
	wall1->model->setShader(ResourceManager::GetShader("model"));
	this->objects.push_back(wall1);

	GameObject* wall2 = new GameObject();
	wall2->setModel(ghostmodel);
	wall2->model->setShader(ResourceManager::GetShader("model"));
	wall2->kinematic.Position = glm::vec3(-5.0f, 0.0f, 5.0f);
	this->objects.push_back(wall2);

	GameObject* wall3 = new GameObject();
	wall3->setModel(ghostmodel);
	wall3->model->setShader(ResourceManager::GetShader("model"));
	wall3->kinematic.Position = glm::vec3(-4.0f, 0.0f, 5.0f);
	this->objects.push_back(wall3);

	GameObject* wall4 = new GameObject();
	wall4->kinematic.Position = glm::vec3(-1.0f, 0.0f, 3.0f);
	wall4->setModel(ghostmodel);
	wall4->model->setShader(ResourceManager::GetShader("model"));
	this->objects.push_back(wall4);
	// Players
	player = new GameObject();
	player->setModel(ghostmodel);
	player->model->setShader(ResourceManager::GetShader("model"));
	player->kinematic.Position = glm::vec3(0.0f, 0.0f, 0.0f);
	this->objects.push_back(player);

	
	GameObject* enemy = new GameObject();
	enemy->setMesh((Mesh*)boxMesh);
	enemy->kinematic.Position = glm::vec3(-3.0f, 0.0f, 0.0f);
	this->objects.push_back(enemy);

	/*
	ObstacleAvoidance* behaviour2 = new ObstacleAvoidance(player, collisions, 2.0f, 10.0f);
	player->behaviours.push_back((Behaviour*) behaviour2);

	std::list<GameObject*> collisions;
	collisions.push_back(wall1);
	collisions.push_back(wall2);
	collisions.push_back(wall3);
	collisions.push_back(wall4);
	*/

	LookWhereYouAreGoing* behaviour3 = new LookWhereYouAreGoing(player, 15.0f, 6.0f, 2.0f, 5.0f, 0.1f);
	player->behaviours.push_back((Behaviour*) behaviour3);

	
	wall1->addBehaviour((Behaviour*) new Separation(wall1, this->objects, 5.0f, 2.0f, 5.0f));
	wall2->addBehaviour((Behaviour*) new Separation(wall2, this->objects, 5.0f, 2.0f, 5.0f));
	wall3->addBehaviour((Behaviour*) new Separation(wall3, this->objects, 5.0f, 2.0f, 5.0f));
	wall4->addBehaviour((Behaviour*) new Separation(wall4, this->objects, 5.0f, 2.0f, 5.0f));


	wall1->addBehaviour((Behaviour*) new DynamicPursue(wall1, player, 1.0f, 2.0f));
	wall2->addBehaviour((Behaviour*) new DynamicPursue(wall2, player, 1.0f, 2.0f));
	wall3->addBehaviour((Behaviour*) new DynamicPursue(wall3, player, 1.0f, 2.0f));
	wall4->addBehaviour((Behaviour*) new DynamicPursue(wall4, player, 1.0f, 2.0f));

	wall1->addBehaviour((Behaviour*) new LookWhereYouAreGoing(wall1, 15.0f, 6.0f, 2.0f, 5.0f, 0.1f));
	wall2->addBehaviour((Behaviour*) new LookWhereYouAreGoing(wall2, 15.0f, 6.0f, 2.0f, 5.0f, 0.1f));
	wall3->addBehaviour((Behaviour*) new LookWhereYouAreGoing(wall3, 15.0f, 6.0f, 2.0f, 5.0f, 0.1f));
	wall4->addBehaviour((Behaviour*) new LookWhereYouAreGoing(wall4, 15.0f, 6.0f, 2.0f, 5.0f, 0.1f));

	enemy->addBehaviour((Behaviour*) new LookWhereYouAreGoing(enemy, 15.0f, 6.0f, 2.0f, 5.0f, 0.1f));
	enemy->addBehaviour((Behaviour*) new Separation(enemy, this->objects, 5.0f, 5.0f, 5.0f));

	//LookWhereYouAreGoing* behaviour3 = new LookWhereYouAreGoing(player, 15.0f, 4.0f, 2.0f, 5.0f, 0.1f);
	//player->behaviours.push_back((Behaviour*)behaviour3);

	//Wander* behaviour2 = new Wander(player, 0.0f, 5.0f, 0.5f, 0.0f, 10.0f, 1.0f, 1.0f, 2.0f, 10.0f, 0.2f);
	//DynamicPursue* behaviour2 = new DynamicPursue(player, enemy, 1.0f, 2.0f);
	//DynamicArrive* behaviour2 = new DynamicArrive(player, enemy, 1.0f, 10.0f, 1.5f, 4.0f, 0.1f);
	//Align* behaviour2 = new Align(player, enemy, 2.0f, 5.0f, 3.0f, 10.0f, 0.1f);
	//Face* behaviour2 = new Face(player, enemy, 1.0f, 3.0f, 1.0f, 10.0f, 0.05f);

	/* Spline Behaviours */
	//splineBehaviour = new PathFollowing(player, spline, 3.0f, 0.015f, 3.0f, 2.0f, 0.2f, 1.0f, 0.1f);
	//player->behaviours.push_back((Behaviour*) splineBehaviour);
}

	// GameLoop
void Game::Update(GLfloat dt) {
	// render
	// ------
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Camera 
	glm::mat4 projection = glm::perspective(glm::radians(camera->Zoom), (float)Width / (float)Height, 0.1f, 100.0f);
	ResourceManager::GetShader("box").Use().setMat4("projection", projection);
	ResourceManager::GetShader("lines").Use().setMat4("projection", projection);
	ResourceManager::GetShader("model").Use().setMat4("projection", projection);
	// camera/view transformation
	glm::mat4 view = camera->GetViewMatrix();
	ResourceManager::GetShader("box").Use().setMat4("view", view);
	ResourceManager::GetShader("lines").Use().setMat4("view", view);
	ResourceManager::GetShader("model").Use().setMat4("view", view);
}

void Game::Render() {
	for (GameObject* i : this->objects) {
		i->Draw(glm::vec3(0.5f,0.5f,0.5f));
	}
}

void Game::PhysicsLoop(GLfloat dt) {
	for (GameObject* i : this->objects) {
		i->getSteering();
		i->kinematic.Position += i->kinematic.Velocity * dt;
		i->kinematic.Orientation += i->kinematic.Rotation * dt;

		i->kinematic.Velocity += i->steering.Linear * dt;
		i->kinematic.Rotation += i->steering.Angular * dt;

		// Clip to max speed
		if (glm::abs(glm::length(i->kinematic.Velocity)) > this->maxSpeed) {
			i->kinematic.Velocity = glm::normalize(i->kinematic.Velocity) * maxSpeed;
		}
	}
}

void Game::DrawPoint(int x, int y) {
	if (splineBehaviour) {
		glm::vec4 worldPos = camera->GetWorldPos(x, y, Width, Height);

		std::cout << worldPos.x << " - " << worldPos.y << " - " << worldPos.z << " - " << worldPos.w << std::endl;

		if (splineBehaviour->path->size > 7) {
			splineBehaviour->path->popPoint3();
		}

		std::cout << "CAMERA: " << camera->Position.x << "-" << camera->Position.y << "-" << camera->Position.z << std::endl;
		splineBehaviour->path->addPoint3(worldPos.x, 0.0f, worldPos.z);
		splineBehaviour->UpdatePath();
		splineBehaviour->path->updateRenderData();
	}
}

void Game::ProcessMovement(Camera_Movement direction, float deltaTime)
{
	float velocity = 10 * deltaTime;
	if (direction == FORWARD)
		player->kinematic.Position += glm::vec3(0.0f, 0.0f, -1.0f) * velocity;
	if (direction == BACKWARD)
		player->kinematic.Position -= glm::vec3(0.0f, 0.0f, -1.0f)  * velocity;
	if (direction == LEFT)
		player->kinematic.Position -= glm::vec3(1.0f, 0.0f, 0.0f) * velocity;
	if (direction == RIGHT)
		player->kinematic.Position += glm::vec3(1.0f, 0.0f, 0.0f) * velocity;

}