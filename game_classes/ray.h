#ifndef RAY_H
#define RAY_H
#include "../headers/resource_manager.h"
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/intersect.hpp>
#include "Debug\debug.h"

class Ray {
public:
	glm::vec3 origin;
	glm::vec3 direction;

	Ray(glm::vec3 origin, float orientation) : origin(origin) {
		direction = asVector(orientation);
	}

	Ray(glm::vec3 origin, glm::vec3 orientation) : origin(origin), direction(orientation){
	}

	glm::vec3 GetPoint(float distance) {
		return origin + direction * distance;
	}

	glm::vec3 getNormalPlane(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec3 cut) {
		glm::vec3 normal = glm::normalize(glm::cross(p2 - p1, p3 - p2));
		if (glm::length(origin - (cut + normal)) < glm::length(origin - (cut - normal))) return normal;
		return -normal;
	}

	void DrawRay(float distance, glm::vec3 color = glm::vec3(1.0f)) {
		GLuint VBO, VAO;
		glm::vec3 endRay = GetPoint(distance);
		float vertices[] = {
			origin.x, origin.y, origin.z,
			endRay.x, endRay.y, endRay.z
		};

		// setting vao vbo
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);

		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(0);

		glm::mat4 model;

		ResourceManager::GetShader("lines").Use().setVec4("ourColor", glm::vec4(color, 1.0f));
		ResourceManager::GetShader("lines").Use().setMat4("model", model);

		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);
	}

};
/*
struct Collision {
	glm::vec3 normal = glm::vec3();
	glm::vec3 point = glm::vec3();
};
static Collision getCollision(Ray ray, float radius, std::list<GameObject*> collisionables)
{

	Collision collision   = Collision();
	glm::vec3 normal      = glm::vec3();
	glm::vec3 innerNormal = glm::vec3();

	for (GameObject* object : collisionables) {
		std::vector<glm::vec3> vertices = object->mesh->vertices;
		float closest = (float)INT_MAX;
		for (int i = 0; i < vertices.size(); i += 3) {
			// Getting all triangles of the mesh
			glm::vec3 p1 = vertices[i] + object->kinematic.Position;
			glm::vec3 p2 = vertices[i+1] + object->kinematic.Position;
			glm::vec3 p3 = vertices[i+2] + object->kinematic.Position;
			glm::vec3 cut;
			if (glm::intersectRayTriangle(ray.origin, ray.direction, p1, p2, p3, cut)) {
				float z = 1.0 - cut.x - cut.y;
				cut = p1 * z + p2 * cut.x + p3 * cut.y;
				float distance = glm::length(ray.origin - cut);
				if (distance < closest && distance < radius)
				{
					Debug::DrawLine(p1, p2, glm::vec3(0.0f, 1.0f, 0.0f));
					Debug::DrawLine(p2, p3, glm::vec3(0.0f, 1.0f, 0.0f));
					Debug::DrawLine(p3, p1, glm::vec3(0.0f,1.0f,0.0f));
					closest = distance;
					innerNormal = ray.getNormalPlane(p1, p2, p3, cut);
					collision.point = cut;
				}
			}
			
		}
		collision.normal += innerNormal;
	}

	return collision;
}
*/
#endif