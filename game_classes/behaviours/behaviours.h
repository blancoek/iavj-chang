#ifndef BEHAVIOURS_H
#define BEHAVIOURS_H

#include "../ray.h"
#include "../../headers/game_object.h"
#include "../splines.h"
#include <GLFW/glfw3.h>

class DynamicSeek : public Behaviour {
public:
	GameObject* target;
	float maxAcceleration;

	DynamicSeek(GameObject* character, GameObject* target, float maxAcceleration);
	SteeringOutput getSteering();
};

class DynamicFlee : public Behaviour {
public:
	GameObject* target;
	float maxAcceleration;

	DynamicFlee(GameObject* character, GameObject* target, float maxAcceleration);
	SteeringOutput getSteering();
};

class DynamicArrive : public Behaviour {
public:
	GameObject* target;
	float maxAcceleration;
	float maxSpeed;
	float targetRadius;
	float slowRadius;
	float timeToTarget = 0.1;

	DynamicArrive(GameObject* character, GameObject* target, float maxAcceleration, float maxSpeed, float targetRadius, float slowRadius, float timeToTarget = 0.1);
	SteeringOutput getSteering();

};

class DynamicVelocityMatching : public Behaviour {
public:
	GameObject* target;
	float maxAcceleration;
	float timeToTarget = 0.1f;

	DynamicVelocityMatching(GameObject* character, GameObject* target, float maxAcceleration, float timeToTarget = 0.1f);
	SteeringOutput getSteering();
};

class Align : public Behaviour {
public:
	GameObject* target;
	float maxAngular;
	float maxRotation;
	float targetRadius;
	float slowRadius;
	float timeToTarget = 0.1f;

	Align(GameObject* character, GameObject* target, float maxAngular, float maxRotation, float targetRadius, float slowRadius, float timeToTarget = 0.1f);
	SteeringOutput getSteering();
};

class Separation : Behaviour {
public:
	float maxAcceleration;
	std::list<GameObject*> collisionables;
	float threshold, decayCoefficient;
	Separation(GameObject* character, std::list<GameObject*> &collisionables, float maxAcceleration, float threshold, float decayCoefficient);
	SteeringOutput getSteering();
};


/*
class ObstacleAvoidance : Behaviour {
public:
float avoidDistance;
float maxAcceleration;
std::list<GameObject*> collisionables;

ObstacleAvoidance(GameObject* character, std::list<GameObject*> &collisionables, float avoidDistance, float maxAcceleration) : avoidDistance(avoidDistance), maxAcceleration(maxAcceleration) {
this->character = character;
this->collisionables = collisionables;
}

SteeringOutput getSteering() {
float time = glfwGetTime();
SteeringOutput steering = SteeringOutput();

Ray ray1 = Ray(character->kinematic.Position, character->kinematic.Orientation);
Ray ray2 = Ray(character->kinematic.Position, character->kinematic.Orientation + sin(15*time)*0.7f);
Ray ray3 = Ray(character->kinematic.Position, character->kinematic.Orientation - sin(15*time)*0.7f);

std::vector<Collision> normals;
normals.push_back(getCollision(ray1, avoidDistance, collisionables));
normals.push_back(getCollision(ray2, avoidDistance*0.66, collisionables));
normals.push_back(getCollision(ray3, avoidDistance*0.66, collisionables));
ray1.DrawRay(avoidDistance);
ray2.DrawRay(avoidDistance*0.66, glm::vec3(1.0f, 1.0f, 0.0f));
ray3.DrawRay(avoidDistance*0.66, glm::vec3(1.0f,1.0f,0.0f));


for (auto collision : normals)
{
if (collision.normal != glm::vec3())
{
Ray(collision.point, glm::reflect(ray1.direction, collision.normal)).DrawRay(avoidDistance, glm::vec3(0.0f, 0.0f, 1.0f)); //Debug Draw ray
//Debug.DrawRay(collision.point, Vector3.Reflect(ray1.direction, collision.normal) * avoidDistance, Color.red);
//steering.linear = Vector3.Reflect(ray1.direction, collision.normal) * kinematic.velocity.magnitude/2;

steering.Linear = glm::normalize(glm::reflect(ray1.direction, collision.normal)) * maxAcceleration;
return steering;
}
}
return steering;
}
};
*/

#endif