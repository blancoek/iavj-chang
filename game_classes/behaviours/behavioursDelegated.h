#ifndef BEHAVIOURS_D_H
#define BEHAVIOURS_D_H
#include "behaviours.h"
class Face : public Align {
public:
	Face(GameObject* character, GameObject* target, float maxAngular, float maxRotation, float targetRadius, float slowRadius, float timeToTarget = 0.1f)
		: Align(character, target, maxAngular, maxRotation, targetRadius, slowRadius, timeToTarget = 0.1f) {
	}

	SteeringOutput getSteering() {
		SteeringOutput steering = SteeringOutput();
		glm::vec3 direction;
		direction = this->target->kinematic.Position - this->character->kinematic.Position;

		if (glm::length(direction) == 0)
			return steering;

		//Guardar valores anteriores
		float orientation = this->target->kinematic.Orientation;
		this->target->kinematic.Orientation = glm::atan(direction.x, direction.z);
		//Calcular steering
		steering = Align::getSteering();

		//Regresar valores anteriores
		this->target->kinematic.Orientation = orientation;
		return steering;
	}
};

class LookWhereYouAreGoing : public Align {
public:
	LookWhereYouAreGoing(GameObject* character, float maxAngular, float maxRotation, float targetRadius, float slowRadius, float timeToTarget = 0.1f)
		: Align(character, new GameObject(), maxAngular, maxRotation, targetRadius, slowRadius, timeToTarget = 0.1f) {
	}

	SteeringOutput getSteering() {
		SteeringOutput steering = SteeringOutput();
		if (glm::length(character->kinematic.Velocity) < glm::epsilon<float>()) {
			character->kinematic.Rotation = 0.0f;
			return steering;
		}
		this->target->kinematic.Orientation = glm::atan(character->kinematic.Velocity.x, character->kinematic.Velocity.z);

		return Align::getSteering();
	}
};

class DynamicPursue : public DynamicSeek {
public:
	float maxPrediction;

	DynamicPursue(GameObject* character, GameObject* target, float maxAcceleration, float maxPrediction)
		: DynamicSeek(character, target, maxAcceleration) {
		this->maxPrediction = maxPrediction;
	}

	SteeringOutput getSteering() {
		float distance, speed, prediction;

		glm::vec3 direction = this->target->kinematic.Position - this->character->kinematic.Position;
		distance = glm::length(direction);
		speed = glm::length(this->character->kinematic.Velocity);

		if (speed <= distance / maxPrediction) prediction = maxPrediction;
		else prediction = distance / speed;


		target->kinematic.Position += target->kinematic.Velocity * prediction;
		SteeringOutput steering = DynamicSeek::getSteering();
		target->kinematic.Position -= target->kinematic.Velocity * prediction;
		return steering;
	}
};

class DynamicEvade : public DynamicFlee {
public:
	float maxPrediction;

	DynamicEvade(GameObject* character, GameObject* target, float maxAcceleration, float maxPrediction)
		: DynamicFlee(character, target, maxAcceleration) {
		this->maxPrediction = maxPrediction;
	}

	SteeringOutput getSteering() {
		float distance, speed, prediction;

		glm::vec3 direction = -this->target->kinematic.Position + this->character->kinematic.Position;
		distance = glm::length(direction);
		speed = glm::length(this->character->kinematic.Velocity);

		if (speed <= distance / maxPrediction) prediction = maxPrediction;
		else prediction = distance / speed;


		target->kinematic.Position += target->kinematic.Velocity * prediction;
		SteeringOutput steering = DynamicFlee::getSteering();
		target->kinematic.Position -= target->kinematic.Velocity * prediction;
		return steering;
	}
};

class Wander : public Face {
public:
	float wanderOffset, wanderRadius, wanderRate, wanderOrientation, maxAcceleration;
	Wander(GameObject* character, float wanderOffset, float wanderRadius, float wanderRate, float wanderOrientation, float maxAcceleration, float maxAngular, float maxRotation, float targetRadius, float slowRadius, float timeToTarget = 0.1f) :
		Face(character, new GameObject(), maxAngular, maxRotation, targetRadius, slowRadius, timeToTarget = 0.1f) {
		this->wanderOffset = wanderOffset;
		this->wanderRadius = wanderRadius;
		this->wanderRate = wanderRate;
		this->wanderOrientation = wanderOrientation;
		this->maxAcceleration = maxAcceleration;
		//this->target->setMesh(ResourceManager::GetMesh("box"));
	}

	SteeringOutput getSteering() {
		float targetOrientation;
		this->wanderOrientation += randomBinomial() * wanderRate;
		targetOrientation = this->wanderOrientation + this->character->kinematic.Orientation;
		this->target->kinematic.Position = this->character->kinematic.Position + this->wanderOffset * asVector(this->character->kinematic.Orientation);
		this->target->kinematic.Position += this->wanderRadius * asVector(targetOrientation);

		//this->target->DrawSprite(ResourceManager::GetTexture("box"), glm::vec3(0.0f, 1.0f, 0.0f));

		SteeringOutput steering = Face::getSteering();
		steering.Linear = this->maxAcceleration * asVector(this->character->kinematic.Orientation);
		return steering;
	}
};

class PathFollowing : public DynamicArrive {
public:
	Spline* path;
	float currentParam;
	float positionOffset;
	float pathOffset;

	PathFollowing(GameObject* character, Spline* path, float positionOffset, float pathOffset, float maxAcceleration, float maxSpeed, float targetRadius, float slowRadius, float timeToTarget = 0.1)
		: DynamicArrive(character, new GameObject(), maxAcceleration, maxSpeed, targetRadius, slowRadius, timeToTarget) {
		this->path = path;
		this->pathOffset = pathOffset;
		this->positionOffset = positionOffset;
		//this->target->setMesh(ResourceManager::GetMesh("box"));
		UpdatePath();
	}

	void UpdatePath() {
		currentParam = path->getParam(character->kinematic.Position);
		target->kinematic.Position = path->getPoint(currentParam);
	}

	SteeringOutput getSteering() {
		float distance = glm::length(character->kinematic.Position - target->kinematic.Position);
		if (currentParam < 0.99 && distance < positionOffset) {
			currentParam += pathOffset / path->size;
			target->kinematic.Position = path->getPoint(currentParam);
		}

		//this->target->Draw(glm::vec3(0.0f, 1.0f, 0.0f));
		path->Draw(glm::vec3(1.0f, 1.0f, 1.0f));

		return DynamicArrive::getSteering();
	}
};
#endif