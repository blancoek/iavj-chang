#ifndef G_MESH_H
#define G_MESH_H

#include "mesh.h"

class CubeMesh : public Mesh {
public:
	CubeMesh(Shader &shader, int drawType=GL_TRIANGLES, int drawSize=36);
	void initRenderData();
};
#endif