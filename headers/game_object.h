#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glad/glad.h>
#include "../game_classes/model.h"
#include <list>

class Behaviour;
struct Kinematic {
	glm::vec3 Position, Velocity = glm::vec3();
	float Orientation, Rotation = 0.0f;
};

struct SteeringOutput {
	glm::vec3 Linear = glm::vec3();
	float Angular = 0.0f;
};

class GameObject
{
public:
	// Object state
	char*		   tag;
	Kinematic      kinematic;
	SteeringOutput steering;
	std::list<Behaviour*> behaviours = std::list<Behaviour*>();	// Every object in the game
	glm::vec3 Size = glm::vec3(1.0f, 1.0f, 1.0f);
	bool		alive = true;
	bool		is_model = false;

	// Constructor(s)
	GameObject();
	GameObject(glm::vec3 pos, glm::vec3 velocity = glm::vec3(0.0f, 0.0f, 0.0f), GLfloat orientation = 0.0f, GLfloat rotation = 0.0f,
		glm::vec3 linear = glm::vec3(0.0f, 0.0f, 0.0f), GLfloat angular = 0.0f);

	void getSteering();
	void resetSteering();
	void setMesh(Mesh* mesh);
	void setModel(Model* model);
	void Draw(glm::vec3 color);
	void addBehaviour(Behaviour* behaviour);
	Mesh* mesh;
	Model* model;
};

class Behaviour {
public:
	GameObject* character;
	virtual SteeringOutput getSteering() = 0;
};

#endif
