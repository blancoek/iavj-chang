#ifndef GAME_H
#define GAME_H

#include "headers/camera.h"
#include <GLFW/glfw3.h>
#include <list>
#include "headers/stb_image.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "headers/shader_c.h"
#include <iostream>
#include <memory>
#include <time.h>
// Behaviours
#include "game_classes\splines.h"
#include "game_classes\ray.h"
#include "game_classes\Mesh\Headers\geometricmesh.h"
#include "game_classes\behaviours\behavioursDelegated.h"

#include "headers/resource_manager.h"

class Game
{
public:
	// Game state
	GLuint  Width, Height;
	std::list<GameObject*> objects = std::list<GameObject*>();	// Every object in the game
	GLFWwindow* window;
	Camera* camera;
	GameObject* player;
	float maxSpeed;
	float maxAcceleration;
	// Constructor/Destructor
	Game(GLuint width, GLuint height, float maxSpeed, float maxAcceleration);
	~Game();
	// Initialize game state (load all shaders/textures/levels)
	void Init();
	void setCamera(Camera& camera);
	// GameLoop
	void Update(GLfloat dt);
	void Render();
	void PhysicsLoop(GLfloat dt);
	void DrawPoint(int x, int y);
	void ProcessMovement(Camera_Movement movement, float dt);

};

#endif