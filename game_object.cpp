#include "headers/game_object.h"


GameObject::GameObject() {
	this->kinematic = { glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), 0.0f, 0.0f };
	this->steering = { glm::vec3(0.0f, 0.0f, 0.0f), 0.0f };
}

GameObject::GameObject(glm::vec3 pos, glm::vec3 velocity, GLfloat orientation, GLfloat rotation, glm::vec3 linear, GLfloat angular) {
	this->kinematic = { pos, velocity, orientation, rotation };
	this->steering = { linear, angular };
};

void GameObject::getSteering()
{
	resetSteering();
	for (auto behaviour : this->behaviours) {
		SteeringOutput new_steering = behaviour->getSteering();
		steering.Linear += new_steering.Linear;
		steering.Angular += new_steering.Angular;
	}
}

void GameObject::resetSteering() {
	this->steering.Linear = glm::vec3();
	this->steering.Angular = 0.0f;
}

void GameObject::addBehaviour(Behaviour* behaviour) {
	this->behaviours.push_back(behaviour);
}

void GameObject::setMesh(Mesh* mesh) {
	this->mesh = mesh;
}

void GameObject::setModel(Model* model) {
	this->model = model;
	is_model = true;
}

void GameObject::Draw(glm::vec3 color)
{
	if (is_model) model->Draw(this->Size, this->kinematic.Position, this->kinematic.Orientation);
	else this->mesh->DrawMesh(this->Size, this->kinematic.Position, this->kinematic.Orientation, color);
}